# atcf file handling

read/write atcf files 

## installation

`pip install atcf`


## format

<https://www.nrlmry.navy.mil/atcf_web/docs/database/new/abdeck.txt>


## files

### analysis

<https://ftp.emc.ncep.noaa.gov/wd20vxt/hwrf-init/decks/>
<https://www.nrlmry.navy.mil/atcf_web/docs/tracks/2018/>

### forecast

atcf like :  <https://manati.star.nesdis.noaa.gov/TC_cone_info/>  (TODO : reader not implemented)
nhc ( not worldwide ) : <ftp://ftp.nhc.noaa.gov/atcf/fst/>

worldwide forecast should be available from jtwc on demand 


## example

```python
import atcf
# read file
df=atcf.read(file)

# linearize dataframe so 1 date is only one line
dflin=atcf.linearize(df)

# back to std
df1=atcf.delinearize(dflin)

# to kml (return fast kml object)
kml=atcf.to_kml(df,radius=34)
```

see also ```atcf2kml.py --help``` for converting atcf tracks to kml

