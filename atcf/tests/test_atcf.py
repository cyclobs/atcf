import unittest
import atcf
import os
import filecmp
import hashlib

class TestAtcf(unittest.TestCase):
    def setUp(self):
        self.atcfPath = "/home/datawork-cersat-public/cache/project/sarwing/users/oarcher/hurricane_coverage/tracks"
        self.atcfFiles = ["bep252018.dat", "bal062016.dat", "bwp242015.dat", "bsh092017.dat"]
    
    def test_read_write_consistency(self):
        for f in self.atcfFiles:
            fullPath = os.path.join(self.atcfPath, f)
            tempPath = os.path.join("/tmp", f)
            df = atcf.read(fullPath)
            with open(tempPath, 'w+') as tempFile:
                atcf.write(df, tempFile)
                with open(fullPath, 'r') as origFile:
                    origFile.seek(0)
                    tempFile.seek(0)
                    hash1 = hashlib.md5(origFile.read().strip().encode('utf-8'))
                    hash2 = hashlib.md5(tempFile.read().strip().encode('utf-8'))
                    self.assertEqual(hash1.hexdigest(), hash2.hexdigest(), f)
    
    def test_linearize(self):
        for f in self.atcfFiles:
            fullPath = os.path.join(self.atcfPath, f)
            lf = atcf.linearize(atcf.read(fullPath))
            self.assertGreater(len(lf.index), 0)
            
    def test_delinearize(self):
        for f in self.atcfFiles:
            fullPath = os.path.join(self.atcfPath, f)
            lf = atcf.linearize(atcf.read(fullPath))
            df = atcf.delinearize(lf)
            self.assertGreater(len(df.index), 0)
            
    def test_getName(self):
        for f in self.atcfFiles:
            fullPath = os.path.join(self.atcfPath, f)
            name = atcf.getName(atcf.read(fullPath))
            print(name)
            self.assertGreater(len(name), 0, name)
            
if __name__ == '__main__':
    unittest.main()

