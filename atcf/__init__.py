#!/usr/bin/env python
from __future__ import unicode_literals
from __future__ import absolute_import

from future import standard_library

standard_library.install_aliases()
from past.builtins import basestring
import pandas as pd
import io
import logging
import os
import math
import sys
import time
import collections
import datetime as dt
import numpy as np

try:
    from StringIO import StringIO  # for Python 2
except ImportError:
    from io import StringIO  # for Python 3

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

cols = ['BASIN', 'CY', 'date', 'TECHNUM/MIN', 'TECH', 'TAU', 'lat', 'lon', 'VMAX', 'MSLP', 'TY', 'RAD', 'WINDCODE',
        'RAD1', 'RAD2', 'RAD3', 'RAD4', 'POUTER', 'ROUTER', 'RMW', 'GUSTS', 'EYE', 'SUBREGION', 'MAXSEAS', 'INITIALS',
        'DIR', 'SPEED', 'STORMNAME', 'DEPTH', 'SEAS', 'SEASCODE', 'SEAS1', 'SEAS2', 'SEAS3', 'SEAS4', 'USERDEFINED1',
        'userdata1', 'USERDEFINED2', 'userdata2', 'USERDEFINED3', 'userdata3', 'USERDEFINED4', 'userdata4',
        'USERDEFINED5', 'userdata5']
cols_fix = ["basin", "CY", "date", "fix format", "fix type", "center/intensity", "flagged indicator", "lat", "lon",
            "height of ob", "posit confidence", "wind speed", "confidence", "pressure", "pressure confidence",
            "pressure derivation", "rad", "windcode", "rad1", "rad2", "rad3", "rad4",
            "radmod1", "radmod2", "radmod3", "radmod4", "radii confidence", "MRD", "eye diameter", "subregion",
            "fix site", "initials"]
cols_fix_type = {"basin": str, "CY": "Int64", "fix format": "Int64", "fix type": str, "center/intensity": str,
                 "flagged indicator": str, "height of ob": "Int64", "posit confidence": "Int64", "wind speed": float,
                 "confidence": "Int64", "pressure": "Int64", "pressure confidence": "Int64", "pressure derivation": str,
                 "rad": "Int64", "windcode": str, "rad1": float, "rad2": float, "rad3": float, "rad4": float,
                 "radmod1": str, "radmod2": str, "radmod3": str, "radmod4": str, "radii confidence": "Int64",
                 "MRD": "Int64", "eye diameter": "Int64", "subregion": str, "fix site": str, "initials": str}

fix_lin_cols = ["basin", "CY", "date", "fix format", "fix type", "center/intensity", "flagged indicator", "lat",
                "lon",
                "height of ob", "posit confidence", "wind speed", "confidence", "pressure", "pressure confidence",
                "pressure derivation",
                'rad_34_neq', 'rad_34_seq', 'rad_34_swq', 'rad_34_nwq',
                'rad_50_neq', 'rad_50_seq', 'rad_50_swq', 'rad_50_nwq',
                'rad_64_neq', 'rad_64_seq', 'rad_64_swq', 'rad_64_nwq',
                "rad_34_neq_mod", "rad_34_nwq_mod", "rad_34_seq_mod", "rad_34_swq_mod",
                "rad_50_neq_mod", "rad_50_nwq_mod", "rad_50_seq_mod", "rad_50_swq_mod",
                "rad_64_neq_mod", "rad_64_nwq_mod", "rad_64_seq_mod", "rad_64_swq_mod",
                "rad_34_confidence", "rad_50_confidence", "rad_64_confidence", "MRD", "eye_diameter", "subregion",
                "fix site", "initials"]

fix_lin_cols_types = {"basin": str, "CY": "Int64", "fix format": "Int64", "fix type": str, "center/intensity": str,
                      "flagged indicator": str, "height of ob": "Int64", "posit confidence": "Int64",
                      "wind speed": float,
                      "confidence": "Int64", "pressure": "Int64", "pressure confidence": "Int64",
                      "pressure derivation": str,
                      'rad_34_neq': float, 'rad_34_seq': float, 'rad_34_swq': float, 'rad_34_nwq': float,
                      'rad_50_neq': float, 'rad_50_seq': float, 'rad_50_swq': float, 'rad_50_nwq': float,
                      'rad_64_neq': float, 'rad_64_seq': float, 'rad_64_swq': float, 'rad_64_nwq': float,
                      "rad_34_neq_mod": str, "rad_34_nwq_mod": str, "rad_34_seq_mod": str, "rad_34_swq_mod": str,
                      "rad_50_neq_mod": str, "rad_50_nwq_mod": str, "rad_50_seq_mod": str, "rad_50_swq_mod": str,
                      "rad_64_neq_mod": str, "rad_64_nwq_mod": str, "rad_64_seq_mod": str, "rad_64_swq_mod": str,
                      "rad_34_confidence": "Int64", "rad_50_confidence": "Int64", "rad_64_confidence": "Int64",
                      "MRD": "Int64", "eye diameter": "Int64", "subregion": str, "fix site": str, "initials": str}

cols_fix_analysis = ["analyst initials", "start time", "end time", "distance to nearest data", "SST",
                     "observation sources", "comments"]
cols_fix_analysis_types = {"analyst initials": str, "distance to nearest data": "Int64", "SST": "Int64",
                           "observation sources": str, "comments": str}


def to_kml(*args, **kwargs):
    from .kml import df2kml
    return df2kml(*args, **kwargs)


def strpos2flt(strpos):
    sect = strpos[-1]
    pos = float("%s.%s" % (strpos[0:-2], strpos[-2]))

    if sect == 'W' or sect == 'S':
        pos = -pos
    return pos


def strpos2flt_fix(strpos):
    if strpos is None or strpos == "" or len(strpos) <= 2:
        return np.nan
    sect = strpos[-1]
    pos = float("%s.%s" % (strpos[0:-2], strpos[-2]))

    if sect == 'W' or sect == 'S':
        pos = -pos
    return pos


def io_size(s_io):
    pos = s_io.tell()
    s_io.seek(0, os.SEEK_END)
    end = s_io.tell()
    s_io.seek(pos)
    return end


def convert_types(df, col_types):
    # Convert columns to specified types
    for c, t in col_types.items():
        # print(df[c])
        if t == float or t == "Int64":
            # pd.isna is set because we want to replace Int64 <NA> to np.nan
            # so that df[c] = df[c].astype("float") succeed
            df.loc[(df[c] == "") | (df[c] is None) | (df[c] == "None") |
                   (df[c] == pd.NA) | pd.isna(df[c]), c] = np.nan
        if t == "Int64":
            # Known pandas bug : https://github.com/pandas-dev/pandas/issues/25472
            df[c] = df[c].astype("float")
        df[c] = df[c].astype(t)

    return df


def read(btfid, model="besttrack", model_fallback=None, max_nb_lines=10000, multi_return=False):
    """
    Read an ATCF BestTrack file, or a Forecast if a model is specified.
    input format : https://www.nrlmry.navy.mil/atcf_web/docs/database/new/abdeck.txt
    exemple BestTrack file : ftp://ftp.emc.ncep.noaa.gov/wd20vxt/hwrf-init/decks/bal142018.dat
    exemple Forecast file : ftp://ftp.nhc.noaa.gov/atcf/fst/al012018.fst

    Parameters
    ==========
    btfid : str Path to file to read
    model : str Format of file. Accepted values are besttrack, fix, forecast. Defaults to besttrack.
    multi_return: bool Only useful if model="fix". If True, will return the "analysis" dataframe, which keeps only
    the lines where "fix type" == "ANAL" and uses additional columns that are specific to that fix type.

    return:
      dataframe
    """

    model_accepted = [None, "forecast", "fix", "besttrack"]
    if model not in model_accepted:
        logger.error(f"Parameter model value {model} not accepted.")

    # Converter used to transform some column's values during the reading of a file
    converters = {
        'date': lambda x: dt.datetime.strptime(x, '%Y%m%d%H'),
        'lat': strpos2flt,
        'lon': strpos2flt
    }

    # The lines able to monitor the time needed for each step have been commented
    # start_time = time.time()

    # Verify if the program is reading a forecast
    if model == "forecast" and isinstance(btfid, str):
        # Retrieve only the lines containing the model name specified
        with open(btfid, 'r') as f:
            # 10000 is the maximum number of lines read at the end of the file by default
            q = collections.deque(f, max_nb_lines)
            # Select only the line with the willed model of forecasting
            #  grep ECMF ash072020.dat > stringIO.csv en bash : real 0m0,014s - user 0m0,000s - sys 0m0,007s
            count_model = 0
            csv_output = StringIO()
            for line in q:
                if model in line:
                    csv_output.write(line)
                    count_model += 1
            logger.debug("Number of lines containing the model name {model} : {count}"
                         .format(count=count_model, model=model))
            # time_to_retrieve_model = time.time()
            # logger.info("Time taken to retrieve model lines in one file is : {}s".format(time_to_retrieve_model-start_time))

            # Treat differently the cases where the model name is OFCL or ECMF
            if count_model == 0:
                if model_fallback != None:
                    logger.debug("No occurrences of {model} in {file}, try with {model_fallback}"
                                 .format(file=os.path.basename(btfid), model=model, model_fallback=model_fallback))
                    df_model_fallback = read(btfid, model_fallback)
                    # transform_one_file_with_model_change = time.time()
                    # logger.info( "Time to transform one file in a pandas DataFrame if the model has been changed is : {}s".format(transform_one_file_with_model_change - start_time))
                    return df_model_fallback
                else:
                    logger.warning("The model name {model} has no occurrence in {file}"
                                   .format(model=model, file=os.path.basename(btfid)))
                    sys.exit(1)
            else:
                # Create a Pandas DataFrame from the csv file
                csv_output.seek(0)
                btfid = csv_output

    if model == "forecast" or model == "besttrack":
        # Create the DataFrame
        df = pd.read_csv(btfid, on_bad_lines='skip', names=cols, index_col=False, sep=',\s*', converters=converters,
                         engine='python')
    elif model == "fix":
        def to_dt(str_time):
            if str_time is None or str_time == "":
                return None
            else:
                return dt.datetime.strptime(str_time, '%Y%m%d%H%M')

        # Converter used to transform some column's values during the reading of a file
        converters = {
            'date': to_dt,
            # 'start time': lambda x: dt.datetime.strptime(x, '%Y%m%d%H%M'),
            # 'end time': lambda x: dt.datetime.strptime(x, '%Y%m%d%H%M'),
            'lat': strpos2flt_fix,
            'lon': strpos2flt_fix
        }
        # Create the DataFrame
        df = pd.read_csv(btfid, on_bad_lines='skip', names=cols_fix, index_col=False, sep=',\s*',
                         converters=converters, engine='python', encoding="ISO-8859-1",
                         keep_default_na=False)

        # Some "rad" are set to "A", which is unexpected (should be 34, 50, 64) and raises a type error.
        # This may tell something, but I have no clue right now.
        # '/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/atcf_fixes/2010/fsh072010.dat'
        df.loc[df["rad"] == "A", "rad"] = ""

        df = convert_types(df, cols_fix_type)

        # TODO the resulting dataframe (df) only contains data from the common section of the fix
        # df_analysis contains additionnal data for rows with fix type == "ANAL"
        # check https://www.nrlmry.navy.mil/atcf_web/docs/database/new/newfdeck.txt

        if multi_return:
            df_analysis = pd.read_csv(btfid, on_bad_lines='skip', names=cols_fix + cols_fix_analysis,
                                      index_col=False, sep=',\s*',
                                      converters=converters, engine='python', encoding="ISO-8859-1",
                                      keep_default_na=False)
            df_analysis = df_analysis.loc[df_analysis["fix type"] == "ANAL"]
            df_analysis.loc[df_analysis["rad"] == "A", "rad"] = ""
            df_analysis = convert_types(df_analysis, {**cols_fix_type, **cols_fix_analysis_types})

            return df, df_analysis

    # transform_one_file = time.time()
    # logger.info("Time to transform one file in a pandas DataFrame is : {}s".format(transform_one_file-start_time))

    return df


def get_start_and_stop_date(btfid, model="besttrack"):
    # Create a txt file with the first and the last line
    csv_output = StringIO()
    with open(btfid, 'r') as f:
        if io_size(f) == 0:
            raise IOError(f"File {btfid} is empty")

        try:
            first_line = next(f)
            csv_output.write(first_line)
        except:
            logger.warning("The file {} is empty, no first line was found".format(btfid))
        try:
            last_line = collections.deque(f, 1).pop()
            csv_output.write(last_line)
        except:
            logger.debug("The file {} is empty or has only one line".format(btfid))
    csv_output.seek(0)
    # Get a DataFrame from the StringIO
    df = read(csv_output, model=model)
    # Get start date and stop date
    start_date = df["date"].min()
    stop_date = df["date"].max()

    return start_date, stop_date


def selecting_files_recently_updated(filepaths, nb_days_to_update, date_of_comparison=dt.datetime.now(),
                                     model="besttrack"):
    new_list = []
    # Test all the atcf files in the list
    for f in filepaths:
        # === From the System Date === #
        # Get the files where the last modification date respect the condition passed in parameter
        # correspond to the value "nb_days_to_update" passed in parameter
        #     if os.path.getmtime(my_path + f) >= date_of_comparison - dt.timedelta(days=nb_days_to_update):
        #         new_list.append(f)

        # === From the Stop Date in file === #
        try:
            start_date, stop_date = get_start_and_stop_date(f, model=model)

            # Get the files where the last modification date respect the condition passed in parameter
            if not pd.isna(stop_date) and not pd.isna(start_date) and \
                    stop_date >= dt.datetime.now() - dt.timedelta(days=nb_days_to_update):
                new_list.append(f)
        except IOError as e:
            logger.warning(f"Skipping file {f} because of error : {e}")
    logger.debug("New list after selecting only the file updated since {last_update} is: {list}".format(
        last_update=dt.datetime.now() - dt.timedelta(days=nb_days_to_update), list=new_list))

    # Return the list containing all the files selected
    return new_list


def selecting_files_in_interval(filepaths, start_date, stop_date=dt.datetime.now()):
    new_list = []
    # Test all the atcf files in the list
    for f in filepaths:
        # Get the start and stop date in file
        start_date_file, stop_date_file = get_start_and_stop_date(f)
        # Select the files where the start date is in the interval specified
        if not pd.isna(stop_date) and not pd.isna(start_date) and start_date <= stop_date_file <= stop_date:
            new_list.append(f)
    logger.debug(
        "New list after selecting only the file between start date {start} and stop date {stop} is: {list}".format(
            start=start_date, stop=stop_date, list=new_list))

    # Return the list containing all the files selected
    return new_list


def write(df, btfid, model="besttrack"):
    """
    write a best-track to a file.
    """

    if model == "besttrack":
        # use a new df for converters:
        df1 = df.copy(deep=True)

        df1.loc[:, 'lat'] = df1['lat'].map(
            lambda l: "%3.0fN" % (float(l) * 10) if float(l) >= 0 else "%3.0fS" % (-float(l) * 10))
        df1.loc[:, 'lon'] = df1['lon'].map(
            lambda l: "%3.0fE" % (float(l) * 10) if float(l) >= 0 else "%3.0fW" % (-float(l) * 10))

        df1.loc[:, 'SEAS'] = df1['SEAS'].map(lambda l: str(int(float(l))) if not np.isnan(l) else "")
        df1.loc[:, 'SEAS1'] = df1['SEAS1'].map(lambda l: str(int(float(l))) if not np.isnan(l) else "")
        df1.loc[:, 'SEAS2'] = df1['SEAS2'].map(lambda l: str(int(float(l))) if not np.isnan(l) else "")
        df1.loc[:, 'SEAS3'] = df1['SEAS3'].map(lambda l: str(int(float(l))) if not np.isnan(l) else "")
        df1.loc[:, 'SEAS4'] = df1['SEAS4'].map(lambda l: str(int(float(l))) if not np.isnan(l) else "")
    elif model == "fix":
        # use a new df for converters:
        df1 = df.copy(deep=True)
        df1.loc[:, 'lat'] = df1['lat'].map(
            lambda l: "%3.0fN" % (float(l) * 10) if float(l) >= 0 else "%3.0fS" % (-float(l) * 10))
        df1.loc[:, 'lon'] = df1['lon'].map(
            lambda l: "%3.0fE" % (float(l) * 10) if float(l) >= 0 else "%3.0fW" % (-float(l) * 10))
    else:
        logger.error(f"Unknown model : '{model}'. Nothing will be written")
        raise ValueError

    if sys.version_info[0] == 3:
        tmpout = io.StringIO()
    else:
        tmpout = io.BytesIO()

    df1.to_csv(tmpout, encoding='utf-8', sep=str(','), header=False, index=False, date_format="%Y%m%d%H")

    btfid.write(tmpout.getvalue().replace(',', ', '))

    tmpout.close()


def linearize(df, drop_seas=True, model="besttrack", lin_cols=None):
    """
    for one date, only one line
    """
    if model == "besttrack":
        # 'RAD', 'WINDCODE'  : supressed NEQ, SEQ                                                                                                                                                                                                                                              dropped  'SEAS', 'SEASCODE', 'SEAS1', 'SEAS2', 'SEAS3', 'SEAS4'
        lincols = ['BASIN', 'CY', 'date', 'TECHNUM/MIN', 'TECH', 'TAU', 'lat', 'lon', 'VMAX', 'MSLP', 'TY',
                   'RAD_34_NEQ',
                   'RAD_34_SEQ', 'RAD_34_SWQ', 'RAD_34_NWQ', 'RAD_50_NEQ', 'RAD_50_SEQ', 'RAD_50_SWQ', 'RAD_50_NWQ',
                   'RAD_64_NEQ', 'RAD_64_SEQ', 'RAD_64_SWQ', 'RAD_64_NWQ', 'POUTER', 'ROUTER', 'RMW', 'GUSTS', 'EYE',
                   'SUBREGION', 'MAXSEAS', 'INITIALS', 'DIR', 'SPEED', 'STORMNAME', 'DEPTH', 'SEAS_12_NEQ',
                   'SEAS_12_SEQ',
                   'SEAS_12_SWQ', 'SEAS_12_NWQ', 'USERDEFINED1', 'userdata1', 'USERDEFINED2', 'userdata2',
                   'USERDEFINED3',
                   'userdata3', 'USERDEFINED4', 'userdata4', 'USERDEFINED5', 'userdata5']
        sects = ['NEQ', 'SEQ', 'SWQ', 'NWQ']
        rad_name = "RAD"
    elif model == "fix":
        if lin_cols is None:
            lincols = fix_lin_cols
        else:
            lincols = lin_cols

        sects = ['neq', 'seq', 'swq', 'nwq']
        windcode = "windcode"
        rad_name = "rad"
        radii_confidence = "radii confidence"
    else:
        msg = f"Unsupported model: {model}"
        logger.error(msg)
        raise ValueError(msg)

    lindf = pd.DataFrame(columns=lincols)

    lastdate = None
    curline = pd.Series(index=lincols)

    if model == "besttrack" and drop_seas != True:
        df.drop(columns=["SEAS", "SEAS1", "SEAS2", "SEAS3", "SEAS4"], inplace=True)

    for index, row in df.iterrows():
        if lastdate and (row['date'] != lastdate):
            lindf = lindf.append(curline, ignore_index=True)
            curline = pd.Series(index=lincols)

        rad = ""
        seas = ""
        for col, val in row.items():
            if col in lincols:
                # curline[col]=val
                # if col in ['lat','lon']:
                #    curline[col]=strpos2flt(val)
                # else:
                curline[col] = val
            else:
                if col == rad_name and not pd.isna(val):
                    rad = int(val)
                    # print "current rad : %d" % rad
                elif model == "fix" and col.startswith(rad_name) and "mod" in col and not pd.isna(val):
                    isect = int(col[-1]) - 1
                    sect = sects[isect]
                    curline["%s_%s_%s_mod" % (rad_name, rad, sect)] = val
                elif col.startswith(rad_name) and not pd.isna(val) and "confidence" not in col:
                    if rad != 0:
                        isect = int(col[-1]) - 1
                        sect = sects[isect]
                        curline["%s_%s_%s" % (rad_name, rad, sect)] = val
                        if model == "fix":
                            curline["%s_%s_confidence" % (rad_name, rad)] = row[radii_confidence]
                        # print "RAD_%s_%s = %d" % (rad, sect, val)

                if model == "besttrack" and drop_seas is False:
                    if col == "SEAS":
                        seas = val
                    elif col in ["SEAS1", "SEAS2", "SEAS3", "SEAS4"]:
                        if seas != 0:
                            isect = int(col[-1]) - 1
                            sect = sects[isect]
                            curline["SEAS_%s_%s" % (seas, sect)] = val

        lastdate = row['date']

    lindf = lindf.append(curline, ignore_index=True)

    return lindf


# cols that have been linearized :
# RAD, RAD1, RAD2, RAD3, RAD4
# SEAS, SEAS1, SEAS2, SEAS3, SEAS4
def delinearize(dflin, model="besttrack"):
    if model == "besttrack":
        cols_default = cols
        lincols = ['BASIN', 'CY', 'date', 'TECHNUM/MIN', 'TECH', 'TAU', 'lat', 'lon', 'VMAX', 'MSLP', 'TY',
                   'RAD_34_NEQ',
                   'RAD_34_SEQ', 'RAD_34_SWQ', 'RAD_34_NWQ', 'RAD_50_NEQ', 'RAD_50_SEQ', 'RAD_50_SWQ', 'RAD_50_NWQ',
                   'RAD_64_NEQ', 'RAD_64_SEQ', 'RAD_64_SWQ', 'RAD_64_NWQ', 'POUTER', 'ROUTER', 'RMW', 'GUSTS', 'EYE',
                   'SUBREGION', 'MAXSEAS', 'INITIALS', 'DIR', 'SPEED', 'STORMNAME', 'DEPTH', 'SEAS_12_NEQ',
                   'SEAS_12_SEQ',
                   'SEAS_12_SWQ', 'SEAS_12_NWQ', 'USERDEFINED1', 'userdata1', 'USERDEFINED2', 'userdata2',
                   'USERDEFINED3',
                   'userdata3', 'USERDEFINED4', 'userdata4', 'USERDEFINED5', 'userdata5']
        sects = ['NEQ', 'SEQ', 'SWQ', 'NWQ']
        windcode = "WINDCODE"
        rad = "RAD"
    elif model == "fix":  # FIXME NOT TESTED
        cols_default = cols_fix
        lincols = ["basin", "CY", "date", "fix format", "fix type", "center/intensity", "flagged indicator", "lat",
                   "lon",
                   "height of ob", "posit confidence", "wind speed", "confidence", "pressure", "pressure confidence",
                   "pressure derivation",
                   'rad_34_neq', 'rad_34_seq', 'rad_34_swq', 'rad_34_nwq',
                   'rad_50_neq', 'rad_50_seq', 'rad_50_swq', 'rad_50_nwq',
                   'rad_64_neq', 'rad_64_seq', 'rad_64_swq', 'rad_64_nwq',
                   "rad_34_neq_mod", "rad_34_nwq_mod", "rad_34_seq_mod", "rad_34_swq_mod",
                   "rad_50_neq_mod", "rad_50_nwq_mod", "rad_50_seq_mod", "rad_50_swq_mod",
                   "rad_64_neq_mod", "rad_64_nwq_mod", "rad_64_seq_mod", "rad_64_swq_mod",
                   "radii confidence", "MRD", "eye", "subregion",
                   "fix site", "initials"]
        sects = ['neq', 'seq', 'swq', 'nwq']
        windcode = "windcode"
        rad = "rad"
    else:
        msg = f"Unsupported model: {model}"
        logger.error(msg)
        raise ValueError(msg)

    df = pd.DataFrame(columns=cols_default)
    if model == "besttrack" and "SEAS" not in dflin:
        dflin.loc[:, "SEAS"] = np.nan
        dflin.loc[:, "SEAS1"] = np.nan
        dflin.loc[:, "SEAS2"] = np.nan
        dflin.loc[:, "SEAS3"] = np.nan
        dflin.loc[:, "SEAS4"] = np.nan

    for index, row in dflin.iterrows():
        curline = pd.Series(index=cols_default)
        pushLine = True  # If no RAD exists, push a line with core infos

        interLines = {}
        for col, val in row.items():
            if col in cols_default:
                curline[col] = val
            else:
                # This code portion creates new pd.Series each time we see a different RAD value
                # in the column name. Each new series is populated with the columns values that
                # are common between linearize and delinearize  data structure.
                # The differents RADx values are filled during the iteration of columns
                if (model == "besttrack" and col.startswith("RAD") and not math.isnan(val)) or \
                        (model == "fix" and col.startswith("rad") and not math.isnan(val)):
                    if not math.isnan(val):
                        pushLine = False  # Do not push the line with core info
                        radVal = col.split("_")[1]
                        if radVal not in interLines:
                            interLines[radVal] = pd.Series(index=cols_default)
                            commonCols = set(cols_default).intersection(lincols)
                            interLines[radVal][commonCols] = row[commonCols]
                            interLines[radVal][rad] = int(radVal)
                            interLines[radVal][windcode] = "NEQ"  # Found no examples with different values

                        radNum = sects.index(col.split("_")[2]) + 1
                        if model == "besttrack":
                            interLines[radVal]["RAD%s" % radNum] = int(val)
                        elif model == "fix":
                            interLines[radVal]["rad%s" % radNum] = int(val)

        if model == "besttrack":
            # Parse for SEAS. Only the value 12 has been seen for SEAS so
            # we only delinearize this (simpler).
            for col, val in row.items():
                if col.startswith("SEAS") and not math.isnan(val):
                    seasVal = int(float(col.split("_")[1]))
                    if int(seasVal) != 12:
                        logger.info(
                            "SEAS value is not 12.. It may have several SEAS values, script not made to delinearize this. Exiting.")
                        sys.exit(1)
                    seasNum = sects.index(col.split("_")[2]) + 1
                    seasCode = "NEQ"  # Found no examples with different value
                    # Apply SEAS values to each RAD row
                    for key in interLines:
                        interLines[key]["SEAS"] = int(seasVal)
                        interLines[key]["SEAS%s" % seasNum] = int(val)
                        interLines[key]["SEASCODE"] = seasCode
                    # Apply it to curline in case there's no RAD row
                    curline["SEAS"] = int(seasVal)
                    curline["SEAS%s" % seasNum] = int(val)
                    curline["SEASCODE"] = seasCode

        # Add the lines when each column of the row is parsed
        interLines = collections.OrderedDict(sorted(interLines.items()))
        df = df.append(pd.DataFrame(list(interLines.values()), columns=cols_default), ignore_index=True)
        if pushLine:
            if model == "besttrack":
                # These values are often (if not always) at 0 for their default value
                curline["RAD"] = 0
                curline["RAD1"] = 0
                curline["RAD2"] = 0
                curline["RAD3"] = 0
                curline["RAD4"] = 0
            elif model == "fix":
                curline[rad] = np.nan
                curline["rad1"] = np.nan
                curline["rad2"] = np.nan
                curline["rad3"] = np.nan
                curline["rad4"] = np.nan
            df = df.append(curline, ignore_index=True)

    return df


def getName(df):
    """extract stormname, if possible
    fallback to stormid , ie bbnnnyyyy
    """
    blacklist = [
        "INVEST",
        "ONE",
        "TWO",
        "THREE",
        "FOUR",
        "FIVE",
        "SIX",
        "SEVEN",
        "EIGHT",
        "NINE",
        "TEN",
        "ELEVEN",
        "TWELVE",
        "THIRTEEN",
        "FOURTEEN",
        "FIFTEEN",
        "SIXTEEN",
        "SEVENTEEN",
        "EIGHTEEN",
        "NINETEEN",
        "TWENTY",
        "TWENTYONE",
        "TWENTYTWO",
        "TWENTYTHREE",
        "TWENTYFOUR",
        "TWENTYFIVE",
        "TWENTYSIX",
        "TWENTYSEVEN",
        "TWENTYEIGHT",
        "TWENTYNINE",
        "THIRTYONE",
        "THIRTYTWO",
        "THIRTYTREE",
        "THIRTYFOUR",
        "THIRTYFIVE",
        "THIRTYSIX",
        "THIRTYSEVEN",
        "THIRTYEIGHT",
        "THIRTYNINE"]

    # Identify the column containing the STORMNAME
    if 'STORMNAME' in df.columns:
        stromname_col_name = 'STORMNAME'
    elif 'NAME' in df.columns:
        stromname_col_name = 'NAME'
    else:
        logger.warning("No column containing the name of the ATCF track persist in the DataFrame")

    # Get a list of all the names in the STORMNAME column
    names = df.sort_values('VMAX', ascending=False)[stromname_col_name].tolist()
    names = [x for i, x in enumerate(names) if names.index(x) == i]  # uniq
    # names=list(set(df['STORMNAME'].tolist()))

    # Filter the name list with the blacklist + GENESIS to get a real name if it exist. Else, return the ATCF ID
    filtered = [n for n in names if n and isinstance(n, basestring) and not n.startswith('GENESIS') and len(
        [n for m in blacklist if m.startswith(n.replace('-', ''))]) == 0]
    if len(filtered) > 0:
        return filtered[0]
    else:
        try:
            return "%s%02d%s" % (df['BASIN'][0].lower(), df['CY'][0], df['date'][0].strftime("%Y"))
        except:
            logger.warning("unable to get name")
            return "not_named"

# if __name__ == "__main__":
#     # === Getting start and stop date in tracks file === #
#     # Test with Best Track
#     path_to_best_file = "/home3/homedir7/perso/vsilvant/Documents/test/forecast/BEST_TRACKS/bal052019.dat"
#     start_date, stop_date = get_start_and_stop_date(path_to_best_file)
#     logger.info("The start date is : {start}\nThe stop date is : {stop}".format(start=start_date, stop=stop_date))
#
#     # Test with Forecast
#     path_to_forecast_file = "/home3/homedir7/perso/vsilvant/Documents/test/forecast/JTWC/aal052019.dat"
#     start_date, stop_date = get_start_and_stop_date(path_to_forecast_file)
#     logger.info("The start date is : {start}\nThe stop date is : {stop}".format(start=start_date, stop=stop_date))
#
#
#     # === List of files selection === #
#     path_to_test_files = "/home/datawork-cersat-public/cache/project/hurricanes/analysis/merge-tracks-atcf"
#     import glob
#     list_of_test_files = [f for f in glob.glob(path_to_test_files + "/*") if (f.endswith('.dat'))]
#
#     # Test the selection of files based on a comparison date
#     start_time = time.time()
#     list_result_comparison = selecting_files_recently_updated(list_of_test_files, 30, dt.datetime.now())
#     logger.info("The list selected by comparison is : {}".format(list_result_comparison))
#     # Test the selection of files where the start date is contained on an interval of time
#     list_result_interval = selecting_files_in_interval(list_of_test_files, dt.datetime.now() - dt.timedelta(days=1500), dt.datetime.now()- dt.timedelta(days=500))
#     logger.info("he list selected in an interval is : {}".format(list_result_interval))
#
#
#    try:
#        import debug
#    except:
#        pass
#    
#    df=read(open(sys.argv[1]))
#    
#    lindf=linearize(df)
#    
#    redf = delinearize(lindf)
#    
#    redf.to_csv("redf.csv")
#    
#    with open("redf.dat", 'w+') as file:
#        write(redf, file)
#    
#    print lindf
#    
#    print lindf.keys()
#    
#    #write(lindf,sys.stdout)
