from setuptools import setup
import glob
import os

setup(name='atcf',
      description='Best track/atcf reading and writing tools',
      url='https://gitlab.ifremer.fr/cyclobs/atcf',
      author = "Theo Cevaer",
      author_email = "Theo.Cevaer@ifremer.fr",
      license='GPL',
      scripts=glob.glob('bin/**'),
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      packages=['atcf'],
      install_requires=[
            'pandas',
            'geopandas',
            'future'
      ],
      test_suite='nose.collector',
      tests_require=['nose'],
      zip_safe=False
      )
