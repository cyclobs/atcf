#!/usr/bin/env python
import sys,os
import argparse
import atcf
import logging    

                            
    #print df.columns
    #print df
    
if __name__ == "__main__":
    logging.basicConfig()
    logger = logging.getLogger(os.path.basename(__file__))
    logger.setLevel(logging.INFO)
    
    description = "Convert atcf storms tracks into kml files"

    parser = argparse.ArgumentParser(description = description)

    parser.add_argument("--radius",action="store",default='34', nargs='?', type=str,help="Radius used for plot. ie eye,64,50,34. use ',' for multiple, ie --radius=eye,64")
    parser.add_argument("--cones",action="store_true",help="add forecast probability cones to radius (not yet implemented)")
    parser.add_argument("--acq",action="store_true",help="show acq/miss if available")
    parser.add_argument("--debug",action="store_true",help="eclipse debug mode")
    
    #parser.add_argument("-o","--out",action="store",default=None, nargs=1,help="Output file (default to stdout)")
    #parser.add_argument("file", help="file to convert")
    parser.add_argument('file', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'),default=sys.stdout)

    args = parser.parse_args()
    
    if args.debug:
        logger.setLevel(logging.DEBUG)
        kml.logger.setLevel(logging.DEBUG)
        import debug
    
    
    df=atcf.read(args.file)
    # kml needs -180 180
    lon=df['lon'].values
    lon[lon>180]=lon[lon>180]-360
    df['lon']=lon
        
    
    kml = atcf.to_kml(df,radius=args.radius.split(','),acq=args.acq)
    logger.info("Exporting into {}".format(args.outfile.name))
    args.outfile.write(kml.to_string(prettyprint=True))
    args.outfile.close()
     
