#!/usr/bin/env python

import argparse
import datetime as dt
import logging
import os
import sys
import time
import glob

import geopandas as gpd
import pandas as pd

import atcf

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def create_big_dataframe(list_of_filenames, model=None, model_fallback=None):
    # Get start time
    start_time = time.time()

    # Concatenate the forecast tracks DataFrame
    print(list_of_filenames)

    if isinstance(list_of_filenames, list) and len(list_of_filenames) != 0:
        try:
        # If no path of locals files are given, create an empty DataFrame
            list_df = []
            # Create a Pandas DataFrame from each csv file
            for filepath in list_of_filenames:
                try:
                    # Log the readed file, the remember start_time
                    logger.info("Reading track: {}".format(os.path.basename(filepath)))

                    # === Read a best-track file === #
                    if model == None:
                        df = atcf.read(filepath)
                        # ATCF format doesn't define 'STORMNAME' for all entry
                        df['STORMNAME'] = atcf.getName(df)

                    # === Read a forecast file === #
                    else:
                        df = atcf.read(filepath, model, model_fallback)
                        #  Select the row with the last date
                        df = df.loc[df['date'] == df['date'].max()]
                        # Transform the DataFrame to correspond to the format willed
                        # TODO : decomment condition
                        #   if df['date'].max() >= dt.datetime.now() - dt.timedelta(days=1):
                        # Update the date by adding the values of "TAU" column to the date of each row
                        for row_index, row in df.iterrows():
                            df.at[row_index, "date"] = row["date"] + dt.timedelta(hours=row['TAU'])
                        # TODO : decomment condition
                        #   else:
                        #       logger.debug("The file {} hasn't been updated for at least one day".format(os.path.basename(filepath)))

                    # Linearize the DataFrame
                    df_linearized = atcf.linearize(df)
                    # Add it's sid corresponding to the atcf name: ie 'wp162019' for file 'bwp162019.dat'
                    df_linearized['sid'] = os.path.splitext(os.path.basename(filepath))[0][1:]

                    #  Append this DataFrame to the list
                    list_df.append(df_linearized)
                    logger.debug("There is some value updated in the track {track} since {last_update}".format(
                        track=filepath, last_update=dt.datetime.now() - dt.timedelta(days=1)))

                except Exception as e:
                    logger.error("Skipping unreadable file {} from list".format(os.path.basename(filepath)))
                    if sys.gettrace():
                        raise e
                    continue

            # Concatenate the list in one big Pandas DataFrame
            if not list_df:
                big_df = pd.DataFrame()
                logger.debug("The list of DataFrame is empty, creating an empty DataFrame")
            else:
                big_df = pd.concat(list_df)

        except:
            logger.error("A problem occurred while creating the big DataFrame")
            sys.exit(1)

        # Log the duration of the merging of all the sources
        stop_time = time.time() - start_time
        logger.info("Read {len} atcf tracks in {time:.2}s ({len_by_time:.2}s / track)".format(
            len=len(list_of_filenames), time=stop_time, len_by_time=stop_time / len(list_of_filenames)))
    else :
        logger.error("The list of path toward the files is empty, or the first argument isn't a list")
        sys.exit(1)

    return big_df


def initialize_lists_from_sources(path_1, path_2, nb_days_to_update=None):
    # === Path 1 files are stored in only one file directory === #
    # Optimise the loading of only recent files
    if nb_days_to_update is not None:
        list_1 = [f for f in glob.glob(path_1 + "/*") if (f.endswith('.dat')
           and int(os.path.basename(f)[5:-4]) >= int((dt.datetime.now() - dt.timedelta(days=nb_days_to_update)).year))]
        list_1 = atcf.selecting_files_recently_updated(list_1, nb_days_to_update)
    # Load all the files
    else:
        list_1 = [f for f in glob.glob(path_1 + "/*") if f.endswith('.dat')]
    # Filter those that are INVEST or TEST, and get only basename
    filepaths_1 = [os.path.basename(f) for f in list_1 if int(os.path.basename(f)[3:-8]) < 80]

    # === Path 2 files are stored in many file directories, each corresponding to a year === #
    # Optimise the loading of only recent files
    if nb_days_to_update is not None:
        list_2 = [f for f in glob.glob(path_2 + "/*/*") if (f.endswith('.dat') and len(os.path.basename(f)) < 14
           and int(os.path.basename(f)[5:-4]) >= int((dt.datetime.now() - dt.timedelta(days=nb_days_to_update)).year))]
        list_2 = atcf.selecting_files_recently_updated(list_2, nb_days_to_update)
    # Load all the files
    else:
        list_2 = [f for f in glob.glob(path_2 + "/*/*") if (f.endswith('.dat') and len(os.path.basename(f)) < 14)]
    # Filter those that are INVEST or TEST, and get only basename
    filepaths_2 = [os.path.basename(f) for f in list_2 if int(os.path.basename(f)[3:-8]) < 80]

    # Common filepaths - contain the files intersection between the two Path
    common_filepaths = list(set(filepaths_1) & set(filepaths_2))
    logger.debug("The list of common tracks between the two sources is:\n{}".format(common_filepaths))
    # Not Common with filepaths 1 - contain the files from Path 1 not intersected between the two Path
    list_unique_source_1 = list(set(filepaths_1).difference(filepaths_2))
    logger.debug("The list of unique tracks in source 1 is:\n{}".format(list_unique_source_1))
    # Not Common with filepaths 2 - contain the files from Path 2 not intersected between the two Path
    list_unique_source_2 = list(set(filepaths_2).difference(filepaths_1))
    logger.debug("The list of unique tracks in source 2 is:\n{}".format(list_unique_source_2))


    # Log some informations
    logger.info("Number of tracks in source 1 is : {s1}, and in source 2 is : {s2}".format(s1=len(filepaths_1),s2=len(filepaths_2)))
    logger.info("Number of unique tracks in source 1 is: {s1}, and in source 2 is : {s2}".format(s1=len(list_unique_source_1),s2=len(list_unique_source_2)))
    logger.info("Number of all common file between the two sources is: {}".format(len(common_filepaths)))
    # Total number of ATCF
    total_number_atcf_read = len(common_filepaths) + len(list_unique_source_1) + len(list_unique_source_2)
    logger.info("The total number of differents Tracks to be updated is: {}".format(total_number_atcf_read))

    # Recreate the lists of files with the whole path to their origin
    list_common_source_1 = [path_best_track_source_1 + "/" + f for f in common_filepaths]
    list_common_source_2 = [path_best_track_source_2 + "/" + os.path.splitext(os.path.basename(f))[0][5:] + "/" + f for f in common_filepaths]
    list_unique_source_1 = [path_best_track_source_1 + "/" + f for f in list_unique_source_1]
    list_unique_source_2 = [path_best_track_source_2 + "/" + os.path.splitext(os.path.basename(f))[0][5:] + "/" + f for f in list_unique_source_2]

    return list_common_source_1, list_common_source_2, list_unique_source_1, list_unique_source_2, total_number_atcf_read


# TODO : suppress this function
# def get_df_from_not_common_files(list_unique_source_1, list_unique_source_2):
#     # Create a big DataFrame with the data present only in the first directory
#     if len(list_unique_source_1) == 0:
#         df_unique_source_1 = pd.DataFrame()
#         logger.info("len(list_unique_source_1) == 0, creating an empty DataFrame\n")
#     else:
#         df_unique_source_1 = create_big_dataframe(list_unique_source_1)
#         # df_unique_source_1 = pd.concat(map(lambda file: atcf.read(path_1), list_unique_source_1))
#         logger.debug("DataFrame from not common filepaths 1 is: {}\n".format(df_unique_source_1))
#
#     # Create a big DataFrame with the data present only in the seconde directory
#     if len(list_unique_source_2) == 0:
#         df_unique_source_2 = pd.DataFrame()
#         logger.info("len(list_unique_source_2) == 0, creating an empty DataFrame\n")
#     else:
#         df_unique_source_2 = create_big_dataframe(list_unique_source_2)
#         logger.debug("DataFrame from not common filepaths 2 is: {}\n".format(df_unique_source_2))
#
#     return df_unique_source_1, df_unique_source_2


def merge_common_files(list_common_source_1, list_common_source_2):
    # TODO : suppress this comment
    # # Concatenate the first common filepaths DataFrame from a unique origin
    #     if len(list_common_source_1) == 0:
    #         big_df_list_common_source_1 = pd.DataFrame()
    #         logger.info("Length of common filepaths 1 is 0, creating an empty DataFrame")
    #     else:
    #         big_df_list_common_source_1 = create_big_dataframe(list_common_source_1)
    #         logger.debug("DataFrame with common filepaths from path 1 is:\n{}".format(big_df_list_common_source_1))
    #
    #     # Concatenate the second common filepaths DataFrame from multiples origins
    #     if len(list_common_source_2) == 0:
    #         big_df_list_common_source_2 = pd.DataFrame()
    #         logger.info("Length of common filepaths 2 is 0, creating an empty DataFrame")
    #     else :
    #         big_df_list_common_source_2 = create_big_dataframe(list_common_source_2)
    #         logger.debug("DataFrame with common filepaths from path 2 is:\n{}".format(big_df_list_common_source_2))
    #
    #     # Merge the two Big Common DataFrame

    # Create the Big Common DataFrames
    big_df_list_common_source_1 = create_big_dataframe(list_common_source_1)
    big_df_list_common_source_2 = create_big_dataframe(list_common_source_2)

    # Concatenate the two Big Common DataFrames
    df_common_merged = pd.concat([big_df_list_common_source_1, big_df_list_common_source_2])
    # TODO : add explanation for this part
    df_common_merged = df_common_merged.reset_index(drop=True)
    df_gpby = df_common_merged.groupby(list(df_common_merged.columns))
    idx = [x[0] for x in df_gpby.groups.values() if len(x) != 1]
    df_common_merged.reindex(idx)

    # Verify which row are duplicated after the concatenation of the two DataFrames
    if logger.isEnabledFor(logging.DEBUG):
        df_common_merged["is_duplicate"] = df_common_merged.duplicated(subset={'date', 'sid'}, keep=False)
        logger.debug("Number of rows duplicated after the concatenation: {}".format(
            len(df_common_merged.loc[df_common_merged['is_duplicate'] == True].index)))
        df_common_merged.drop(columns=["is_duplicate"], inplace=True)

    # Drop duplicates from the concatenation of the two DataFrames
    df_common_merged.drop_duplicates(subset={'date', 'sid'}, keep='first', inplace=True)

    # Count number of unique "sid" after dropping the duplicated rows, just to verify the result
    logger.debug("Number of tracks in the merged result is: {}".format(len(df_common_merged.sid.unique())))

    # Verify from which source the rows are comming
    if logger.isEnabledFor(logging.DEBUG):
        # Find the rows sourced only from the first source
        df_merge_left_only = big_df_list_common_source_1.merge(big_df_list_common_source_2, how='outer', indicator=True).loc[lambda x: x['_merge'] == 'left_only']
        # Find the rows sourced only from the second source
        df_merge_right_only = big_df_list_common_source_1.merge(big_df_list_common_source_2, how = 'outer' ,indicator=True).loc[lambda x: x['_merge']=='right_only']
        # Log the number of rows comming from each sources
        logger.debug("Number of rows in the merged Dataframe from the source 1: {s1}, and the source 2: {s2}".format(
            s1=len(df_merge_left_only.index), s2=len(df_merge_right_only.index)))

    return df_common_merged


def merge_with_forecast(path_forecasts_files, list_current_best_tracks, df_merge, model="OFCL", model_fallback="ECMF"):
    # TODO : Adapt the selection of the forecast file
    # Create list of all forecast and of current best-track
    list_forecasts_tracks = [f for f in glob.glob(path_forecasts_files + "/*")]
    # Select the current forecast
    list_current_forecasts_tracks = []
    for forecast_filename in [os.path.basename(f) for f in list_forecasts_tracks]:
        if forecast_filename in list_current_best_tracks:
            list_current_forecasts_tracks.append(forecast_filename)

    # Create a DataFrame with all the last forecast for each current track
    df_forecast = create_big_dataframe(list_current_forecasts_tracks, model, model_fallback)

    # Concatenate the forcasts with the best-track in a DataFrame
    df_complete = pd.concat([df_merge, df_forecast])

    # Watch if rows are duplicated between the forecasts and the best-tracks
    df_complete["is_duplicate"] = df_complete.duplicated(subset={'date', 'sid'}, keep=False)
    df_duplicated_rows = df_complete.loc[df_complete['is_duplicate'] == True, ['sid', 'date', 'TECH']]
    logger.info("There is {} rows where forecast and best-tracks points are duplicated".format(len(df_duplicated_rows.index)))

    # Drop the forecast if there is a best-track for the same date
    df_complete.drop((df_complete['TECH'] != 'BEST') & (df_complete['is_duplicate'] == True), inplace=True)
    df_complete.drop(columns={"is_duplicate"}, inplace=True)
    logger.info("There is {} track with forecast point".format(len(df_complete.loc[df_complete['TECH'] != "BEST"].sid.unique())))

    # Some model for ATCF Forecast doesn't define 'STORMNAME' by default
    df_complete['STORMNAME'] = atcf.getName(df_complete)

    return df_complete


def write_tracks_in_dat(df, path_output, nb_days_to_update=None):
    # Select the files updated after the timedelta
    if nb_days_to_update is not None:
        df_last_days = df.loc[df['date'] >= dt.datetime.now() - dt.timedelta(days=nb_days_to_update)]
        list_unique_sid = df_last_days.sid.unique()
    # Else, list the sid in all file
    else:
        list_unique_sid = df.sid.unique()

    # Loop on each track present in df_last_days inside the complete DataFrame to update the whole track and not only a part of it
    total_atcf_updated = 0
    for sid in list_unique_sid:
        logger.debug("Write the file b{}.dat".format(sid))

        # Prepare the DataFrame to be written
        df_by_sid = df.loc[df['sid'] == sid]
        delinearized_df_by_sid = atcf.delinearize(df_by_sid)

        # Write the DataFrame in a file
        output_filename = path_output + "b" + sid + ".dat"
        atcf.write(delinearized_df_by_sid, open(output_filename, "w"))
        total_atcf_updated += 1

    return total_atcf_updated


def write_tracks_in_big_files(df, asfile, mode, path_output, by_start_date, write_func, **kwargs):
    # Create a GeoDataFrame
    gdf = gpd.GeoDataFrame(df)

    # Create list of unique year or month to select the interval of date wanted to create a file
    # list_unique_year = gdf['date'].dt.year.unique()  # pd.DatetimeIndex(df_complete['date']).year # df_complete['date'].map(lambda x: x.year)
    # list_unique_month = gdf['date'].dt.month.unique()  # pd.DatetimeIndex(df_complete['date']).month # df_complete['date'].map(lambda x: x.month)

    # Create the Geometry column
    gdf['geometry'] = gpd.points_from_xy(gdf['lon'], gdf['lat'])
    gdf.set_geometry('geometry', inplace=True)

    # Export the data depending on the mode chosen
    if mode == 'year' or mode == 'month' or mode == 'all':
        # Loop on every year
        for year in gdf['date'].dt.year.unique():
            gdf_by_year = gdf.loc[gdf['year'] == year]

            # Writing yearly file
            if mode == 'year' or mode == 'all':
                # Get the whole track if it started in the year given
                if by_start_date == True:
                    list_gdf = []
                    for sid in gdf_by_year.sid.unique():
                        gdf_by_sid = gdf.loc[gdf['sid'] == sid]
                        if gdf_by_sid['date'].dt.year.min(axis=1) == year:
                            list_gdf.append(gdf_by_sid)
                    gdf_by_year = pd.concat(list_gdf)
                # The transform the datetime line to format YYYYMMHH so it can be sorted as a shapefile
                gdf_by_year['date'] = gdf_by_year['date'].apply(lambda x: dt.datetime.strftime(x, '%Y%m%d%H'))
                # Define an index for the GeoDataFrame
                gdf_by_year.set_index(['sid', 'date'], inplace=True)
                # Write the file
                file_output = path_output + "/" + "ATCF_" + str(year) + "." + str(asfile)
                write_func(gdf_by_year, file_output, **kwargs)

            # Writing monthly file
            if mode == 'month' or mode == 'all':
                # Loop on every month
                for month in gdf['date'].dt.month.unique():
                    gdf_by_year_and_month = gdf_by_year.loc[gdf_by_year['month'] == month]
                    # Get the whole track if it started in the month given
                    if by_start_date == True:
                        list_gdf = []
                        for sid in gdf_by_year_and_month.sid.unique():
                            gdf_by_sid = gdf.loc[gdf['sid'] == sid]
                            if gdf_by_sid['date'].dt.month.min(axis=1) == month:
                                list_gdf.append(gdf_by_sid)
                        gdf_by_year_and_month = pd.concat(list_gdf)
                    # The transform the datetime line to format YYYYMMHH so it can be sorted as a shapefile
                    gdf_by_year_and_month['date'] = gdf_by_year_and_month['date'].apply(lambda x: dt.datetime.strftime(x, '%Y%m%d%H'))
                    # Define an index for the GeoDataFrame
                    gdf_by_year_and_month.set_index(['sid', 'date'], inplace=True)
                    # Write the file
                    file_output = path_output + "/" + "ATCF_" + str(year) + str(("%02d" % month)) + "." + str(asfile)
                    write_func(gdf_by_year_and_month, file_output, **kwargs)

    else:
        # Export only the last month data
        year_to_update, month_to_update = 0, 0
        if dt.datetime.now().month > 1:
            month_to_update = dt.datetime.now().month - 1
            year_to_update = dt.datetime.now().year
        else:
            month_to_update = 12
            year_to_update = dt.datetime.now().year - 1

        # TODO : Use this code to add genericity to this function !
        # Create the DataFrame to be written
        gdf_by_year = gdf.loc[gdf['date'].dt.year == year_to_update]
        gdf_by_year_and_month = gdf_by_year.loc[gdf_by_year['date'].dt.month == month_to_update]
        # Get the whole track if it started in the month given
        if by_start_date == True:
            list_gdf = []
            logger.debug("Number of unique track to write : {}".format(len(gdf_by_year_and_month.sid.unique())))
            for sid in gdf_by_year_and_month.sid.unique():
                gdf_by_sid = gdf.loc[gdf['sid'] == sid]
                if gdf_by_sid['date'].dt.month.min() == month_to_update:
                    list_gdf.append(gdf_by_sid)
            gdf_by_year_and_month = pd.concat(list_gdf)
        # The transform the datetime line to format YYYYMMHH so it can be sorted as a shapefile
        gdf['date'] = gdf['date'].apply(lambda x: dt.datetime.strftime(x, '%Y%m%d%H'))
        # Define an index for the GeoDataFrame
        gdf.set_index(['sid', 'date'], inplace=True)
        # Write the file
        file_output = path_output + "/" + "ATCF_" + str(year_to_update) + str(("%02d" % month_to_update)) + "." + str(asfile)
        write_func(gdf_by_year_and_month, file_output, **kwargs)


if __name__ == "__main__":
    description = """Merge the two file directories containing ATCF raw data, 
    aiming to create a product we can safely implement in Cyclobs without loosing Tracks"""

    # Get start time
    start_time = time.time()

    # Parse the given arguments when the script is executed
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-f", "--asfile", action="store", type=str, default="dat",
        help="type of file to be written: dat, pickle, shapefile, gpkg or all")
    parser.add_argument("-m", "--mode", action="store", type=str,
        help="content of big file to be written: year, month, last_month or all")
    parser.add_argument("-o", "--path_output", action="store", type=str,
        default="/home/datawork-cersat-public/cache/project/hurricanes/analysis/merge-tracks-atcf/",
        help="specify where the files will be written")
    parser.add_argument("-i", "--include_model_forecast", action="append", type=str,
        help="specify which model will be used from forecast file. Add a second parameter to use a fallback model")
    parser.add_argument("-d", "--nb_days_to_update", action="store", type=int,
        help="number of days needed to update the tracks")
    # By default the big files (gpkg) contains the points recorded during the period specified (month or year)
    parser.add_argument("-s", "--by_start_date", action="store_true", default=False,
        help="if True, the big files (gpkg) will contain the entire tracks that started in a month or a year")
    args = parser.parse_args()

    # Useful variable
    path_best_track_source_1 = "/home/datawork-cersat-public/archive/provider/noaa/mirrors/ftp.emc.ncep.noaa.gov/wd20vxt/hwrf-init/decks"
    path_best_track_source_2 = "/home/datawork-cersat-public/provider/nrl/model/model/hurricane_tracks/nrt"
    # TODO : Update the path of forecast file
    path_forecasts_files = "/home3/homedir7/perso/vsilvant/Documents/test/forecast/JTWC"

    # =========================================================================== #
    # === Initialize the lists containing the files from the two ATCF sources === #
    # =========================================================================== #

    if args.mode == 'last_month':
        args.nb_days_to_update = 31

    list_common_source_1, list_common_source_2, list_unique_source_1, list_unique_source_2, total_number_atcf_read = \
        initialize_lists_from_sources(path_best_track_source_1, path_best_track_source_2, args.nb_days_to_update)

    # ============================================================= #
    # === Create the merged result of all atcf Best-Track files === #
    # ============================================================= #

    # Getting the files not common between the two directories, that needn't to be merged
    df_unique_source_1 = create_big_dataframe(list_unique_source_1)
    df_unique_source_2 = create_big_dataframe(list_unique_source_2)

    # Merging the data common between the two file directories
    if len(list_common_source_1) == 0 or len(list_common_source_2) == 0:
        df_common_merged = pd.DataFrame()
        logger.info("There is no common file, creating an empty DataFrame")
    else:
        df_common_merged = merge_common_files(list_common_source_1, list_common_source_2)

    # Log the duration of the merging of all the sources
    time_spent_read = time.time() - start_time
    logger.info("Read {len} atcf tracks in {time:.0}s ({len_by_time:.1}s / track)".format(
        len=total_number_atcf_read, time=time_spent_read, len_by_time=time_spent_read / total_number_atcf_read))

    # Concatenate the three Big DataFrames (Common + Source 1 only + Source 2 only)
    df_merge = pd.concat([df_common_merged, df_unique_source_1, df_unique_source_2])

    # ===================================================== #
    # === Create a DataFrame with the current forecasts === #
    # ===================================================== #
    if args.nb_days_to_update is not None and args.include_model_forecast is not None:
        list_current_best_tracks = [os.path.basename(f) for f in list_common_source_1] + \
                                   [os.path.basename(f) for f in list_unique_source_1] + \
                                   [os.path.basename(f) for f in list_unique_source_2]
        df_complete = merge_with_forecast(path_forecasts_files, list_current_best_tracks, df_merge, args.include_model_forecast[0], args.include_model_forecast[1])
    else:
        df_complete = df_merge
        logger.debug("There is no forecast tracks to merge with the best-tracks")

    # ========================================================= #
    # === Write the merged result to the new file directory === #
    # ========================================================= #

    total_atcf_updated = 0

    # === Create a new file for each Track === #
    if args.asfile == 'dat' or args.asfile == 'all':
        total_atcf_updated = write_tracks_in_dat(df_complete, args.path_output, args.nb_days_to_update)

    # === Create big files: shapefile, pickle or geopackage ; containing cyclones' tracks that started in an interval of time === #
    if args.asfile != "dat":
        total_atcf_updated = total_number_atcf_read
        # if the file to be sorted should be a pickle
        if args.asfile == 'pkl' or args.asfile == 'all':
            write_tracks_in_big_files(df_complete, 'pkl', args.mode, args.path_output, args.by_start_date, gpd.GeoDataFrame.to_pickle)

        # if the file to be sorted should be a shapefile
        if args.asfile == 'shp' or args.asfile == 'all':
            # TODO : shorten the columns 'TECHNUM/MIN' 'SEAS_12_NEQ' 'SEAS_12_SEQ' 'SEAS_12_SWQ' 'SEAS_12_NWQ' 'SEAS_nan_NEQ' 'SEAS_nan_NWQ' 'SEAS_nan_SEQ' 'SEAS_nan_SWQ'
            write_tracks_in_big_files(df_complete, 'shp', args.mode, args.path_output, args.by_start_date, gpd.GeoDataFrame.to_file, driver='ESRI Shapefile')

        # if the file to be sorted should be a geopackage
        if args.asfile == 'gpkg' or args.asfile == 'all':
            write_tracks_in_big_files(df_complete, 'gpkg', args.mode, args.path_output, args.by_start_date, gpd.GeoDataFrame.to_file, driver='GPKG')

    # Log the duration of the merging of all the sources
    time_spent_writing = time.time() - start_time
    if total_atcf_updated != 0:
        logger.info("Write {len} atcf tracks in {time:.0}s ({len_by_time:.1}s / track)".format(
            len=total_atcf_updated, time=time_spent_writing, len_by_time=time_spent_writing / total_atcf_updated))
    else:
        logger.warning("total_atcf_updated == 0.")
